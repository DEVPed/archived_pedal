const path = require('path');
const devServer = require('./config/webpack-dev').devServer;
const { paths, rules, plugins, resolve, IS_PRODUCTION, IS_DEVELOPMENT } = require('./config/webpack-common');

const entry = [
  path.join(path.resolve(__dirname, "src"), 'client.js')
];

if (IS_DEVELOPMENT) {
  const addProductionPlugings = require('./config/webpack-dev').addProductionPlugings
  addProductionPlugings(plugins);
} else {
  const addProductionPlugings = require('./config/webpack-production').addProductionPlugings;
  addProductionPlugings(plugins)
}

module.exports = {
  devtool: IS_PRODUCTION ? 'cheap-module-source-map' : 'cheap-eval-source-map',
  context: path.resolve(__dirname, "src"),
  watch: !IS_PRODUCTION,
  entry,
  output: { path: path.resolve(__dirname, "build"), publicPath: '/', filename: paths.output.client },
  module: { rules },
  resolve,
  plugins,
  devServer,
};
