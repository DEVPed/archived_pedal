const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const StyleExtHtmlWebpackPlugin = require('style-ext-html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const autoprefixer = require('autoprefixer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const JavaScriptObfuscator = require('webpack-obfuscator');

const { paths, IS_PRODUCTION, IS_DEVELOPMENT } = require('./webpack-common');
const addProductionPlugings = (plugins) => {
    plugins.push(
        new CopyWebpackPlugin([
            { from: `${paths.app}/index.html`, to: paths.build },
            { from: `${paths.app}/favicon.png`, to: paths.build },
            { from: `${paths.themeAssets}`, to: `${paths.build}assets` },
        ],
            { copyUnmodified: true }
        ),
        new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify('production') }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false, conditionals: true, comparisons: true, sequences: true, dead_code: true, evaluate: true, if_return: true, join_vars: true, unused: true, booleans: true, },
            output: { comments: false }
        }),
        new JavaScriptObfuscator({ rotateUnicodeArray: true }, []),
        new webpack.LoaderOptionsPlugin({
            options: { postcss: [autoprefixer({ browsers: ['last 3 version', 'ie >= 10'] })], context: paths.themePublic }
        }),
        new webpack.HashedModuleIdsPlugin(),
        new ScriptExtHtmlWebpackPlugin({ defaultAttribute: 'defer' }),
        new ExtractTextPlugin({ filename: '[name].[contenthash].css', allChunks: true }),
        new StyleExtHtmlWebpackPlugin({ minify: true }),
        new CompressionPlugin({
            asset: '[path].gz[query]',
            algorithm: 'gzip',
            test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
            threshold: 10240,
            minRatio: 0.8
        })
    )
};

module.exports = {
    addProductionPlugings
}