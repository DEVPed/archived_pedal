const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var randomstring = require("randomstring");
const appSrc = '../src/';
const themeInnerSrc = `${appSrc}./theme-gallery/theme-material-ui/`;
const NODE_ENV = process.env.NODE_ENV || 'development';
const SERVER_RENDER = process.env.SERVER_RENDER === 'true';
const IS_DEVELOPMENT = NODE_ENV === 'development';
const IS_PRODUCTION = NODE_ENV === 'production';

const paths = {
  app: path.join(__dirname, appSrc),
  javascript: path.join(__dirname, `${appSrc}`),
  images: path.join(__dirname, `${appSrc}/assets`),
  theme: path.join(__dirname, themeInnerSrc),
  themePublic: path.join(__dirname, `${themeInnerSrc}public/`),
  themeAssets: path.join(__dirname, `${themeInnerSrc}/assets`),
  themeStyle: path.join(__dirname, `${themeInnerSrc}/styles`),
  build: path.join(__dirname, '../build/'),
  nodeModule: path.join(__dirname, '../node_modules'),
  output: {
    client: `src/client${randomstring.generate()}.js`,
    vendor: `src/vendor${randomstring.generate()}.js`,
    css: `src/style${randomstring.generate()}.css`,
  }
};

const plugins = [
  new ExtractTextPlugin(paths.output.css),
  new webpack.ProvidePlugin({ "window.Tether": 'tether', Popper: ['popper.js', 'default'], }),
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    filename: paths.output.vendor,
    minChunks(module) {
      const context = module.context;
      return context && context.indexOf('node_modules') >= 0;
    },
  }),
  new HtmlWebpackPlugin({
    template: path.join(paths.app, 'index.html'),
    path: paths.build,
    filename: 'index.html',
    minify: {
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
      removeComments: true,
      useShortDoctype: true,
      collapseInlineTagWhitespace: true,
      removeRedundantAttributes: true
    },
  })
]

const rules = [
  {
    enforce: 'pre',
    test: /\.js$/,
    exclude: /(node_modules|bower_components)/,
    include: /src/,
    use: ['eslint-loader']
  },
  {
    test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: ['babel-loader'],
  },
  {
    test: /\.less$/,
    include: paths.themeStyle,
    exclude: /node_modules/,
    use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'less-loader' }, { loader: 'url-loader' }, { loader: 'file-loader' },],
  },
  {
    test: /\.scss$/,
    include: paths.themeStyle,
    exclude: '/node_modules/',
    loader: ExtractTextPlugin.extract({
      use: [
        { loader: 'css-loader', options: { sourceMap: IS_DEVELOPMENT, minimize: true }, },
        { loader: 'postcss-loader', options: { sourceMap: IS_DEVELOPMENT, options: { plugins: function () { return [require('precss'), require('autoprefixer')]; } } }, },
        { loader: 'sass-loader', options: { sourceMap: IS_DEVELOPMENT }, },
      ],
    }),
  },
  {
    test: /\.sass$/,
    include: paths.themeStyle,
    exclude: /node_modules/,
    use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'less-loader' }, { loader: 'url-loader' }, { loader: 'file-loader' }],
  },
  {
    test: /\.css$/,
    include: [paths.themeStyle],
    exclude: /node_modules/,
    use: [{ loader: 'style-loader' }, { loader: 'css-loader' },],
  },
  {
    test: /\.svg$/,
    use: [
      { loader: 'babel-loader', },
      {
        loader: 'react-svg-loader',
        options: { includePaths: [paths.themeAssets], svgo: { plugins: [{ removeTitle: true, },], floatPrecision: 2, }, },
      },
    ],
    include: paths.svg,
  },
  {
    test: /\.(png|jpg|gif|mp4|ogg|svg|woff|woff2|ttf|eot|ico)$/,
    use: [
      {
        loader: 'file-loader',
        options: { name: 'client/assets/[name]-[hash].[ext]', }
      },
      {
        loader: 'url-loader?limit=100000',
        options: { name: 'client/assets/[name]-[hash].[ext]' },
      },
    ],
  },
];

const resolve = {
  extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js', '.jsx', '.min.js'],
  modules: [path.join(__dirname, '../node_modules'), paths.javascript,],
};

module.exports = {
  paths: paths.output,
  paths,
  plugins,
  resolve,
  rules,
  IS_DEVELOPMENT,
  IS_PRODUCTION,
  NODE_ENV,
  SERVER_RENDER,
};
