const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { paths, IS_PRODUCTION, IS_DEVELOPMENT } = require('./webpack-common');

const addProductionPlugings = (plugins) => {
  plugins.push(
    new CopyWebpackPlugin([
      { from: `${paths.themeAssets}`, to: `${paths.build}/assets` },
    ],
      { copyUnmodified: true }
    ),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),

  )
};

const devServer = {
  contentBase: IS_PRODUCTION ? paths.build : paths.app,
  historyApiFallback: true,
  port: 3000,
  compress: IS_PRODUCTION,
  inline: !IS_PRODUCTION,
  hot: !IS_PRODUCTION,
  host: 'localhost',
  disableHostCheck: true,
  overlay: { warnings: true, errors: true },
  stats: {
    children: false,
    assets: true,
    modules: false,
    publicPath: false,
    chunks: false,
    hash: false,
    timings: true,
    version: false,
    warnings: true,
  },
};

module.exports = {
  devServer,
  addProductionPlugings
};
