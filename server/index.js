
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const requestPromise = require('request-promise');
const serverUrl = "http://54.72.93.87:8080";
var portHttp = 3001;
var portProxy = 8080;

// init http server
//-------------------------------------------------------------
const httpApp = express();
httpApp.use(bodyParser.json({ limit: '5mb' }));
httpApp.use(bodyParser.urlencoded({ extended: false }));
httpApp.use(cookieParser());
httpApp.use(express.static('./build'));

var httpServer = http.createServer(httpApp);

httpServer.listen(portHttp, function () {
  console.log('Web Server port ' + portHttp + '!')
});


// init proxy server server
var proxyApp = express();
proxyApp.use(bodyParser.json({ limit: '5mb' }));

proxyApp.use(bodyParser.urlencoded({ extended: false }));
proxyApp.use(cookieParser());
proxyApp.all("/[^(mock)]*", (req, res, next) => proxyCall(req, res, next));
proxyApp.all("/mock/*", (req, res, next) => mockCall(req, res, next));

var proxyServer = http.createServer(proxyApp);
proxyServer.listen(portProxy, function () {
  console.log('Proxy Server listening on port ' + portProxy + '!');
});

function mockCall(req, res, next) {
  const origin = req.headers.origin || "GET";
  res.set('Access-Control-Allow-Origin', origin);
  res.set('Access-Control-Allow-Headers', "*");
  res.set('Access-Control-Allow-Methods', "*");
  res.set('Content-Type', 'text/json');
  res.send({ status: 'ok', type: 'mock', url: req.originalUrl });
  return res.end();
}


function proxyCall(req, res, next) {
  // TODO add url console
  // store original origin
  const origin = req.headers.origin || "localhost";
  delete req.headers.origin;

  const _responseHandler = (response) =>{ return responseHandler(response, req, res, next, origin)};
  const _errorHandler = (error) => errorHandler(error, req, res, next, origin);

  requestPromise({
    uri: serverUrl + req.originalUrl,
    method: req.method,
    headers: req.headers,
    timeout: 3000,
    body: ((typeof req.body === 'string') ? req.body : JSON.stringify(req.body)),
    resolveWithFullResponse: true,
  })
  .then(response => { return _responseHandler(response)})
  .catch(error => _errorHandler(error));
}

const responseHandler = function (response, req, res, next, origin) {

    for (var [key, value] of Object.entries(response.headers)) {
      if (key !=='transfer-encoding') res.set(key, value);
    }
    res.set('Access-Control-Allow-Origin', origin);
    if (response.headers['allow']) {
      res.set('Access-Control-Allow-Methods', response.headers['allow']);
    }
    if (response.headers['access-control-request-headers']) {
      res.set('Access-Control-Allow-Headers', response.headers['access-control-request-headers']);
    } else if (req.headers['access-control-request-headers']) {
      res.set('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
    }
    res.statusCode = response.statusCode;
    res.send(response.body);
    return response;
}


const errorHandler = function (error, req, res, next, origin) {
    if (error.response) {
      for (var [key, value] of Object.entries(response.headers)) {
        if (key !=='transfer-encoding') res.set(key, value);
      }
      res.set('Access-Control-Allow-Origin', origin);
      res.statusCode = error.response.statusCode;
      res.send(error.response.body);
    } else {
      res.set('Access-Control-Allow-Origin', origin);
      res.set('Content-Type', 'text/json');
      res.statusCode = 500;
      res.send({ status: 'error', message: error.message });
    }
    return error;
}
