import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import ResetPassword from '~/src/theme-gallery/theme-material-ui/pages/ResetPassword';
import { selectors } from '~/src/stores/selector'
import { injectIntl } from 'react-intl';

import { loginApi, getHealth, setLanguage } from '~/src/actions/GeneralActions'

const selector = formValueSelector('resetPasswordForm')

const mapStateToProps = (state) => {
  return ({
  });
}

const mapDispatchToProps = (dispatch) => ({
});

const resetPasswordForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPassword);

export default injectIntl(reduxForm({ form: 'resetPasswordForm' })(resetPasswordForm));