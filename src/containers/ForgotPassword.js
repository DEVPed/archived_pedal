import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import ForgotPassword from '~/src/theme-gallery/theme-material-ui/pages/ForgotPassword';
import { selectors } from '~/src/stores/selector'
import { injectIntl } from 'react-intl';

import { loginApi, getHealth, setLanguage } from '~/src/actions/GeneralActions'

const selector = formValueSelector('forgotPasswordForm')

const mapStateToProps = (state) => {
  return ({
  });
}

const mapDispatchToProps = (dispatch) => ({
});

const forgotPasswordForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPassword);

export default injectIntl(reduxForm({ form: 'forgotPasswordForm' })(forgotPasswordForm));