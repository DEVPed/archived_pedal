import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import Login from '~/src/theme-gallery/theme-material-ui/pages/Login';
import { selectors } from '~/src/stores/selector'
import { injectIntl } from 'react-intl';

import { loginApi, getHealth, setLanguage } from '~/src/actions/GeneralActions'

const selector = formValueSelector('loginForm')

const mapStateToProps = (state) => {
  return ({
    emailValues: selector(state, 'email'),
    passwordValues: selector(state, 'password'),
    loginData: selectors.login(state.generalReducer),
    healthData: selectors.getHealthData(state.generalReducer),
    language: selectors.getLanguage(state.generalReducer)
  });
}

const mapDispatchToProps = (dispatch) => ({
  doLogin: (obj) => dispatch(loginApi(obj)),
  getHealth: (obj) => dispatch(getHealth()),
  setLanguage: (language) => dispatch(setLanguage(language)),
});

const formLogin = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

export default injectIntl(reduxForm({ form: 'loginForm' })(formLogin));