import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import Dashoard from '~/src/theme-gallery/theme-material-ui/pages/dashboard/Dashboard';
import { selectors } from '~/src/stores/selector';

const mapStateToProps = (state) => {
	return {
		isLoading: state.generalReducer.isLoading
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
	};
};

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Dashoard));