import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import ConfirmOTP from '~/src/theme-gallery/theme-material-ui/pages/ConfirmOTP';
import { selectors } from '~/src/stores/selector'
import { injectIntl } from 'react-intl';

import {  getHealth, setLanguage } from '~/src/actions/GeneralActions'

const selector = formValueSelector('confirmOTPForm')

const mapStateToProps = (state) => {
  return ({
  });
}

const mapDispatchToProps = (dispatch) => ({
});

const formConfirmOTP = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmOTP);

export default injectIntl(reduxForm({ form: 'confirmOTPForm' })(formConfirmOTP));