import React from 'react';
import { render } from 'react-dom';
import { Route,Redirect, Switch } from 'react-router-dom';
import { createStore, compose, applyMiddleware ,combineReducers} from 'redux';
import { Provider } from 'react-redux';
import createHistory from 'history/createHashHistory';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import reducers from './reducers';
import thunk from 'redux-thunk'
import ls from 'local-storage';
import IntlProviderWrapper from './IntlProviderWrapper';
import App from '~/src/theme-gallery/theme-material-ui/MainApp';
import Page404 from '~/src/theme-gallery/theme-material-ui/pages/404';
import { apiMiddleware } from 'redux-api-middleware';

const history = createHistory();
const middleware = routerMiddleware(history);
const reducer = combineReducers(reducers);
const createStoreWithMiddleware = applyMiddleware(apiMiddleware)(createStore);
const store = createStoreWithMiddleware(
  reducers,
  undefined,
  compose(applyMiddleware(thunk, middleware))
);

render(
  <Provider store={store}>
    <IntlProviderWrapper>
      <ConnectedRouter history={history}>
        <Switch>
          <Route path="/" component={App} />
          <Route path="*" component={Page404}/>
        </Switch>
      </ConnectedRouter>
    </IntlProviderWrapper>
  </Provider>,
  document.getElementById('app-container')
);
