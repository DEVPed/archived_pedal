
import * as types from '~/src/constants/ActionTypes';
import ls from 'local-storage';
import { CALL_API } from 'redux-api-middleware';
import { Schema, arrayOf, normalize } from 'normalizr';
import _ from 'lodash';


export const setLanguage = (language) => ({ type: types.SITE_LANGUAGE, language })
export const loginSuccess = (user) => ({ type: types.LOGIN_SUCCESS, message: null, isLoading: false })
export const loginFailed = (message) => ({ type: types.LOGIN_FAILED, message: message, isLoading: false })
export const requestLogin = (language) => ({ type: types.REQUEST_LOGIN, message: null, isLoading: true })

export function loginApi(obj) {
  return dispatch => {
    dispatch(requestLogin())
    const checkCredentials = (obj.password === 'a' && obj.username === 'a');
    ls.set('logged', checkCredentials);
    return dispatch => checkCredentials ? dispatch(loginSuccess()) : dispatch(loginFailed('wrong login credentials'))
  }
}


export const getHealth = () => ({
  [CALL_API]: {
    endpoint: 'http://localhost:8080/api/v1/health',
    method: 'GET',
    options: { mode: 'cors' },
    headers: { 'Content-Type': 'application/json', 'pragma': 'no-cache', 'cache-control': 'no-cache' },
    types: [
      types.FETCH_HEALTH_CHECK,
      {
        type: types.FETCH_HEALTH_CHECK_SUCCESS,
        payload: (action, state, res) => {
          const contentType = res.headers.get('Content-Type');
          if (contentType && ~contentType.indexOf('json')) {
            // Just making sure res.json() does not raise an error
            return res.json().then((json) => json);
          }
          return res.body;
        }
      },
      {
        type: types.FETCH_HEALTH_CHECK_FAILURE,
        meta: (action, state, res) => {
          if (res) {
            return {
              status: res.status,
              statusText: res.statusText
            };
          } else {
            return {
              status: 'Network request failed'
            }
          }
        }
      },

    ]
  }
});
