import * as types from '~/src/constants/ActionTypes';

const initiateState = {
    type: '',
    language: 'en'
}
const generalReducer = (state = initiateState, action) => {
    switch (action.type) {
        case types.SITE_LANGUAGE: return { ...state, language: action.language };
        case types.FETCH_HEALTH_CHECK: return { ...state, isLoading: true, healthData: null, error: null, status: null };
        case types.FETCH_HEALTH_CHECK_SUCCESS: return { ...state, isLoading: false, healthData: action.payload, error: null, meta: action.meta };
        case types.FETCH_HEALTH_CHECK_FAILURE: return { ...state, isLoading: false, healthData: null, error: action.payload, meta: action.meta };
        case types.REQUEST_LOGIN: return { ...state, isLoading: action.isLoading, message: action.message };
        case types.LOGIN_SUCCESS: return { ...state, isLoading: action.isLoading, message: action.message };
        case types.LOGIN_FAILED: return { ...state, isLoading: action.isLoading, message: action.message };
        default: return state;
    }
};

export default generalReducer;