import * as types  from '~/src/constants/ActionTypes';
const en = require('~/src/locales/languages/en.json');

const initialState = { defaultLocale: 'en', locale: 'en', messages: en }
let messages = { en };

const localesReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CHANGE_LANGUAGE: return { ...state, locale: action.locale, messages: messages[action.locale] }
        default: return state
    }
}

export default localesReducer