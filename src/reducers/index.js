import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
//reducers
import generalReducer from './GeneralReducer';
import localesReducer from './localereducer';


const reducers = {
  routing: routerReducer,
  form: formReducer,
  generalReducer,
  localesReducer
};

export default combineReducers(reducers);
