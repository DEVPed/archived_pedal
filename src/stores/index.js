import { createStore } from 'redux';
import reducers from '~/src/reducers/index';

function reduxStore(initialState) {
  const store = createStore(reducers, initialState,
    window.devToolsExtension && window.devToolsExtension());

  if (module.hot) {
    module.hot.accept('~/src/reducers', () => {
      const nextReducer = require('~/src/reducers/index');
      store.replaceReducer(nextReducer);
    });
  }
  return store;
}
export default reduxStore;
