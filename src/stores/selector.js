export const selectors = {
    login: (state) => state.loginData,
    getHealthData: (state) => state.healthData,
    getLanguage: (state) => state.language,
    
}