import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import * as routes from './RouteLoader';
import ls from 'local-storage';

const RouteWithAuth = ({ component: Component, loading, ...rest }) => {
  return (
      <Route
          {...rest}
          render={(props) => (ls.get('logged') ||  loading)
              ? <Component {...props} />
              : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />}
      />
  )
}
class RouteBuilder extends React.Component {
  render() {
    const { match, history, isLoading } = this.props;
    return (
      <div>
      <Route path={`${match.url}/login`} component={routes.login} />
        <RouteWithAuth loading={isLoading} path={`${match.url}/view-claim`} component={routes.viewClaim} />
        <RouteWithAuth loading={isLoading} path={`${match.url}/dashboard`} component={routes.dashboard} />
       
      </div>
    );
  }
}

module.exports = RouteBuilder;
