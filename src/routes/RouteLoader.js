import loadable from 'react-loadable';
import React from 'react';
const LoadingComponent =() => (<div></div>)

export const login = loadable({
  loader: () => import('../theme-gallery/theme-material-ui/pages/Login'),
  loading: LoadingComponent
})
export const viewClaim = loadable({
  loader: () => import('../theme-gallery/theme-material-ui/pages/view-claim/ViewClaim'),
  loading: LoadingComponent
})
export const dashboard = loadable({
  loader: () => import('../theme-gallery/theme-material-ui/pages/dashboard/Dashboard'),
  loading: LoadingComponent
})
export const resetPassword = loadable({
  loader: () => import('../containers/ResetPassword'),
  loading: LoadingComponent
})
export const forgotPassword = loadable({
  loader: () => import('../containers/ForgotPassword'),
  loading: LoadingComponent
})
export const confirmOTP = loadable({
  loader: () => import('../containers/ConfirmOTP'),
  loading: LoadingComponent
})