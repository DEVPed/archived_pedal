import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import { Route, Switch, Redirect } from 'react-router-dom';
import ls from 'local-storage';
import Reboot from 'material-ui/Reboot';

import Body from './Body';
import Page404 from '~/src/theme-gallery/theme-material-ui/pages/404';
import Login from '~/src/containers/Login';
import ForgotPassword from '~/src/containers/ForgotPassword';
import * as routes from '~/src/routes/RouteLoader';

import './styles/app.scss';

const RouteWithAuth = ({ component: Component, loading, ...rest }) => {
  return (
      <Route
          {...rest}
          render={(props) => (ls.get('logged') ||  loading)
              ? <Component {...props} />
              : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />}
      />
  )
}
class MainApp extends Component {


  theme = createMuiTheme({})
  render() {
    const { match, location } = this.props;

    const isRoot = location.pathname === '/' ? true : false;
    if (isRoot && !ls.get('logged')) {
      return (<Redirect to={'/login'} />);
    }
    if (isRoot) {
      return (<Redirect to={'/app/dashboard'} />);
    }
    return (
      <MuiThemeProvider theme={this.theme}>
        {/* Reboot kickstart an elegant, consistent, and simple baseline to build upon. */}
        <Reboot />
        <RouteWithAuth path={`${match.url}app`} component={Body} />
        <Route exact path="/login" component={Login} />
        <Route path={`/reset-password`} component={routes.resetPassword} />
        <Route path={`/forgot-password`} component={routes.forgotPassword} />
        <Route path={`/confirm-otp`} component={routes.confirmOTP} />
        <Route exact path="/404" component={Page404} />
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({});

module.exports = connect(
  mapStateToProps
)(MainApp);
