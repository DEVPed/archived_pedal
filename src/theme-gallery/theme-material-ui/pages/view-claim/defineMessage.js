import { defineMessages } from 'react-intl';
export const messages = defineMessages({
	headerLabel: {
		id: 'report.header.title',
		defaultMessage: 'View Claim'
	},
})
