import React from 'react';
import { intlShape, injectIntl, FormattedMessage } from 'react-intl';
import { messages } from './defineMessage';



class ViewClaim extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return <div className="page-view-claim">Claim page</div>;
  }
}

ViewClaim.propTypes = {
  intl: intlShape.isRequired,
};

export default ViewClaim