import React from 'react';
import { Field } from 'redux-form';
import Button from 'material-ui/Button';
import { selectors } from '~/src/stores/selector';
import { Link } from 'react-router-dom';
import { intlShape } from 'react-intl';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
    root: {
        height: '100%'
    },
    mainContainer: {
        height: '100%'
    },
    loginContainer: {
        paddingLeft: 20,
        paddingRight: 20,
    },
    backgroundContainer: {
        backgroundColor: '#f6f7f9',
        background: 'url(assets/images/loginBackground.png) repeat',
        backgroundPositionX: 'left',
        backgroundPositionY: 'top',
    },

});

class Login extends React.Component {
    email = (item) => <TextField {...item.input} fullWidth={true} label="E-mail address" margin="normal" />

    password = (item) => <TextField {...item.input} fullWidth={true} label="Password" margin="normal" type="password" variant="password" autoComplete="current-password" />
    submit = (event) => {
        event.preventDefault();
        this.props.doLogin({ username: this.props.emailValues, password: this.props.passwordValues });
        const { history } = this.props;
        history.push('/app/dashboard');
    }

    componentDidMount() {
        this.props.getHealth();
        //this.props.setLanguage('HU');
    }
    render() {
        const { handleSubmit, healthData, language, classes, theme } = this.props
        return (
            <Grid container alignItems="stretch" direction="row" justify="flex-start" className={classes.mainContainer}>
                <Grid item xs={4}>
                    <Grid container alignItems="stretch" direction="column" justify="flex-start" className={classes.mainContainer}>
                        <Grid item>
                            <Typography variant="display2" gutterBottom align="center">
                                Pedal
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="title" gutterBottom align="center">
                                Sign in to your Pedal account
                        </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="subheading" gutterBottom align="center">
                                Join to next generation of vehicle ownership
                            </Typography>
                        </Grid>
                        <Grid item>
                            <form onSubmit={handleSubmit} className="form-horizontal">
                                <Grid container alignItems="stretch" direction="column" justify="center" className={classes.loginContainer}>
                                    <Grid item>
                                        <Field name="email" component={this.email} />
                                    </Grid>
                                    <Grid item>
                                        <Field name="password" component={this.password} />
                                    </Grid>
                                    <Grid item>
                                        <Button variant="raised" type="submit"
                                            color="primary"
                                            fullWidth={true} onClick={this.submit.bind(this)} >Sign in</Button>
                                    </Grid>
                                </Grid>
                            </form>
                        </Grid>
                        <Grid item>
                            <Typography variant="body1" gutterBottom align="center">
                                Can't sign in? <a href="#/forgot-password">Forgotten Password</a>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body1" gutterBottom align="center">
                                By continuing you agree to our<br />
                                <a href="#/login">Terms of service</a>, and <a href="#/login">Privacy policy</a>
                            </Typography>
                        </Grid>




                    </Grid>
                </Grid>
                <Grid key={2} item xs={8} className={classes.backgroundContainer}>
                </Grid>
            </Grid>
        )
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Login);
