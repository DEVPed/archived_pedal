import React from 'react';
import PropTypes from 'prop-types';
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import { withStyles } from 'material-ui/styles';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import classNames from 'classnames';
import Cars from './Cars';

const styles = theme => ({
  expansionPanel: {
    marginBottom: 10
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightMedium,
  },
  summary: {
    fontSize: theme.typography.pxToRem(10),
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.secondary,
  },
  rightCommand: {
    marginRight:-32
  },
});


class BoxRegisteredVehicles extends React.Component {

  render() {
    const { classes, theme } = this.props;

    return (
      <ExpansionPanel className={classNames(classes.expansionPanel, this.props.className)} defaultExpanded={true} expanded={true}>
        <ExpansionPanelSummary>
          <Grid container className={classes.demo} alignItems='flex-end' direction='row' justify='space-between'>
            <Grid key={1} item>
              <Typography className={classes.heading}>Registered Vehicles</Typography>
              <Typography className={classes.summary}>You have 4 vehicles registered</Typography>
            </Grid>
            <Grid key={2} item>
              <Button className={classes.rightCommand}>Add more</Button>
            </Grid>
          </Grid>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Grid container className={classes.demo} alignItems='flex-end' direction='row' justify='space-between'>
            <Grid key={1} item>
              <Cars title="Audi Q5 S line" description="JA4 LS3" filename="car4.png" />
            </Grid>
            <Grid key={2} item>
              <Cars title="2016 Ford Fiesta" description="TYP 808" filename="car5.png" />
            </Grid>
            <Grid key={3} item>
              <Cars title="Audi A4" description="KVF 456" filename="car6.png" />
            </Grid>
            <Grid key={4} item>
              <Cars title="BMW I8 Hybrid" description="E10 WWW" filename="car7.png" />
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

BoxRegisteredVehicles.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(BoxRegisteredVehicles);
