import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import classNames from 'classnames';
import Button from 'material-ui/Button';

const styles = theme => ({
  carTitle: {
    fontSize: theme.typography.pxToRem(12),
    fontWeight: theme.typography.fontWeightMedium,
    textAlign: 'center'
  },
  carSubTitle: {
    fontSize: theme.typography.pxToRem(10),
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.secondary,
    textAlign: 'center'
  },
});

class Cars extends React.Component {
  render() {
    const { classes, theme } = this.props;
    return (
      <Button>
        <Grid container className={classes.demo} direction='column' justify='flex-end' alignItems='center'>
          <Grid key={1} item>
            <img src={"assets/images/" + this.props.filename} width="150" />
          </Grid>
          <Grid key={2} item>
            <Typography className={classes.carTitle}>{this.props.title}</Typography>
            <Typography className={classes.carSubTitle}>{this.props.description}</Typography>
          </Grid>
        </Grid>
      </Button>
    );
  }
}

Cars.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Cars);
