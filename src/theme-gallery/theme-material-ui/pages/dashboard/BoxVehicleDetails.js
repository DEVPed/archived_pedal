import React from 'react';
import PropTypes from 'prop-types';
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import { withStyles } from 'material-ui/styles';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import Typography from 'material-ui/Typography';
import classNames from 'classnames';
import Grid from 'material-ui/Grid';
import grey from 'material-ui/colors/grey';

const styles = theme => ({
  expansionPanel: {
    marginBottom: 10
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightMedium,
  },
  summary: {
    fontSize: theme.typography.pxToRem(10),
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.secondary,
  },
  hr: {
    color: grey.A100,
  },
  vehicleDataName: {
    fontSize: theme.typography.pxToRem(12),
    fontWeight: theme.typography.fontWeightMedium,
  },
  vehicleDataValue: {
    fontSize: theme.typography.pxToRem(12),
    fontWeight: theme.typography.fontWeightRegular,
  },
  vehicleDataContainer: {
    maxHeight: 120
  },
  vehicleDataTitle: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightMedium,
    textAlign:'center',
    width:'100%'
  },
});


class BoxVehicleDetails extends React.Component {

  vehicleData = [
    { itemName: 'VEHICLE TYPE', itemValue: 'CAR' },
    { itemName: 'EMISSION', itemValue: '159 g/Km' },
    { itemName: 'TAX BAND', itemValue: 'G' },
    { itemName: 'MANUFACTURED', itemValue: '2013' },
    { itemName: 'REGISTRATION', itemValue: '11 January 2013' },
    { itemName: 'MILEAGE', itemValue: '29856' },
    { itemName: 'FUEL', itemValue: 'DIESEL' },
    { itemName: 'COLOUR', itemValue: 'BLACK' },
    { itemName: 'ENGINE', itemValue: '1968cc' },
    { itemName: 'TAX BAND', itemValue: 'G' },
    { itemName: '6 Months Tax', itemValue: '£104.50' },
    { itemName: '12 Month Tax', itemValue: '£190' },
    { itemName: 'Vehicle Type', itemValue: 'Car' },
    { itemTitle: 'Renewing soon', },
    { itemName: 'TAX:', itemValue: '01 Sept 2018' },
    { itemName: 'MOT:', itemValue: '09 Aug 2018' },
  ]
  render() {
    const { classes, theme } = this.props;

    return (
      <ExpansionPanel defaultExpanded={true} className={classNames(classes.expansionPanel, this.props.className)}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Grid container alignItems='flex-start' direction='row' justify='flex-start'>
            <Grid key={1} item>
              <Typography className={classes.heading}>VehicleDetails</Typography>
              <Typography className={classes.summary}>AUDI Q5 S Line Plus Tdi Quattro</Typography>
            </Grid>
          </Grid>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Grid container>
            <Grid item xs={12}>
              <hr className={classes.hr} noshade="noshade" size="1" />
              <Typography>£19.785.00 on 15th Oct Condition: Fair - Update</Typography>
              <hr className={classes.hr} noshade="noshade" size="1" />
            </Grid>
            <Grid item xs={12}>
              <Grid container className={classes.vehicleDataContainer} alignItems='stretch' direction='column' justify='flex-start' spacing={8}>
                {this.vehicleData.map(value => (
                  <Grid key={Math.random()} item>
                    <Grid container className={classes.vehicleDataItemContainer} alignItems='flex-start' direction='row' justify='space-between' key={Math.random()}>
                      {!!value.itemName && <Grid item key={Math.random()}>
                        <Typography className={classes.vehicleDataName} key={Math.random()}>{value.itemName}</Typography>
                      </Grid>}
                      {!!value.itemValue && <Grid item key={Math.random()}>
                        <Typography className={classes.vehicleDataValue} key={Math.random()}>{value.itemValue}</Typography>
                      </Grid>}
                      {!!value.itemTitle && <Grid item xs={12} key={Math.random()}>
                        <Typography className={classes.vehicleDataTitle} key={Math.random()}>{value.itemTitle}</Typography>
                      </Grid>}
                    </Grid>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}



BoxVehicleDetails.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(BoxVehicleDetails);
