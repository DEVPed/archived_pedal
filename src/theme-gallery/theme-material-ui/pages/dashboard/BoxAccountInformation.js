import React from 'react';
import PropTypes from 'prop-types';
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import { withStyles } from 'material-ui/styles';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import Typography from 'material-ui/Typography';
import classNames from 'classnames';
import Grid from 'material-ui/Grid';

const styles = theme => ({
  expansionPanel: {
    marginBottom: 10
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightMedium,
  },
  summary: {
    fontSize: theme.typography.pxToRem(10),
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.secondary,
    display: 'block',
  },
});


class BoxYourAccidentClaim extends React.Component {

  render() {
    const { classes, theme } = this.props;

    return (
      <ExpansionPanel className={classNames(classes.expansionPanel, this.props.className)}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Grid container className={classes.demo} alignItems='flex-end' direction='row' justify='space-between'>
            <Grid key={1} item>
              <Typography className={classes.heading}>Account information</Typography>
            </Grid>
          </Grid>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography></Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}



BoxYourAccidentClaim.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(BoxYourAccidentClaim);
