import React from 'react';
import PropTypes from 'prop-types';
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import { withStyles } from 'material-ui/styles';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import Typography from 'material-ui/Typography';
import BoxRegisteredVehicles from './BoxRegisteredVehicles';
import BoxVehicleDetails from './BoxVehicleDetails';
import BoxYourAccidentClaim from './BoxYourAccidentClaim';
import BoxRecentCharges from './BoxRecentCharges';
import BoxRunningCost from './BoxRunningCost';
import BoxYourInsuranceDetails from './BoxYourInsuranceDetails';
import BoxAccountInformation from './BoxAccountInformation';

import {
  FETCH_HEALTH_CHECK,
} from '~/src/constants/ActionTypes';

const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});


class Dashboard extends React.Component {

  componentDidMount() {
  }
  render() {
    const { classes, theme } = this.props;
    return (
      <div className={classes.root} id="page-dashboard">
        <BoxRegisteredVehicles />
        <BoxVehicleDetails />
        <BoxYourAccidentClaim />
        <BoxRecentCharges />
        <BoxRunningCost />
        <BoxYourInsuranceDetails />
        <BoxAccountInformation />
      </div>
    );
  }
}



Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Dashboard);
