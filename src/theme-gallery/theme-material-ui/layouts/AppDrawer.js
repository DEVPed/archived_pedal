import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import { FormControlLabel, FormGroup } from 'material-ui/Form';
import Menu, { MenuItem } from 'material-ui/Menu';
import Divider from 'material-ui/Divider';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Typography from 'material-ui/Typography';
import CloseIcon from 'material-ui-icons/Close';
import InboxIcon from 'material-ui-icons/Inbox';
import DraftsIcon from 'material-ui-icons/Drafts';


const drawerWidth = 240;

const styles = theme => ({
  drawerPaper: {
    position: 'relative',
    height: '100%',
    width: drawerWidth,
  },
  flex: {
    flex: 1,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
});
class AppDrawer extends React.Component {


  render() {
    const { classes, theme, open, handleDrawerClose } = this.props;

    return (
      <Drawer
        variant="persistent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor='left'
        open={open}
        onClick={handleDrawerClose}
        onKeyDown={handleDrawerClose}
      >
        <div className={classes.drawerInner}>
          <div className={classes.drawerHeader}>
            <Typography variant="title" color="inherit" className={classes.flex}>
              Menu
            </Typography>
            <IconButton onClick={handleDrawerClose}>
              <CloseIcon />
            </IconButton>
          </div>
          <Divider />
          <List className={classes.list}>
            <ListItem button onClick={handleDrawerClose} component="a" href="#/app/dashboard">
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary="Dashboard" />
            </ListItem>
            <ListItem button onClick={handleDrawerClose} component="a" href="#/app/view-claim">
              <ListItemIcon>
                <DraftsIcon />
              </ListItemIcon>
              <ListItemText primary="View Claim" />
            </ListItem>
          </List>
          <Divider />
          <List className={classes.list}>
            <ListItem button onClick={handleDrawerClose} component="a" href="#/login">
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary="Login" />
            </ListItem>
            <ListItem button onClick={handleDrawerClose} component="a" href="#/confirm-otp">
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary="Confirm OTP" />
            </ListItem>
            <ListItem button onClick={handleDrawerClose} component="a" href="#/forgot-password">
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary="Forgot Password" />
            </ListItem>
            <ListItem button onClick={handleDrawerClose} component="a" href="#/reset-password">
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary="Reset Password" />
            </ListItem>
          </List>
        </div>
      </Drawer>
    );
  }
}

AppDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

module.exports = withStyles(styles, { withTheme: true })(AppDrawer);
