import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import Menu, { MenuItem } from 'material-ui/Menu';
import ls from 'local-storage';

import Avatar from 'material-ui/Avatar';
import AccountCircle from 'material-ui-icons/AccountCircle';

const styles = theme => ({
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  profileMenu: {
    width:'inherit'
  },
  avatar: {
    margin: 10,
    width: 30,
    height: 30,
  },
});
class AppToolbar extends React.Component {

  state = {
    anchorEl: null,
  };


  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };


  render() {
    const { classes, theme, handleDrawerOpen } = this.props;
    const { anchorEl } = this.state;

    const openProfile = Boolean(anchorEl);

    return (
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="Menu"
          onClick={handleDrawerOpen}
          className={classes.menuButton}>
          <MenuIcon />
        </IconButton>
        <Typography variant="title" color="inherit" className={classes.flex}>
          Pedal
        </Typography>
        <Typography variant="title" color="inherit" className={classes.flex}>
          <img src="assets/images/logo.png" alt="Audi" style={{ height: '30px' }} />
        </Typography>

        {ls.get('logged') && (
          <div>
            <IconButton
              aria-owns={openProfile ? 'menu-appbar' : null}
              aria-haspopup="true"
              onClick={this.handleMenu}
              color="inherit"
              className={classes.profileMenu}
            >
              <Typography variant="subheading" color="inherit">
                Mr Richard Eaton
              </Typography>
              <Avatar alt="Richard Eaton" src="assets/images/avatar.jpg" className={classes.avatar} />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={openProfile}
              onClose={this.handleClose}
            >
              <MenuItem onClick={this.handleClose}>Profile</MenuItem>
              <MenuItem onClick={this.handleClose}>My account</MenuItem>
            </Menu>
          </div>
        )}
      </Toolbar>
    );
  }
}

AppToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

module.exports = withStyles(styles, { withTheme: true })(AppToolbar);
