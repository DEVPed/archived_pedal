import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import classNames from 'classnames';
import RouteBuilder from '~/src/routes/RouteBuilder';
import AppDrawer from './layouts/AppDrawer';
import AppToolbar from './layouts/AppToolbar';


const drawerWidth = 240;

const styles = theme => ({
  root: {
    width: '100%',
    zIndex: 1,
    overflow: 'hidden',
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  appBar: {
    position: 'absolute',
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'appBarShift-left': {
    marginLeft: drawerWidth,
  },
  content: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    height: 'calc(100% - 56px)',
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      marginTop: 64,
    },
  },
  'content-left': {
    marginLeft: -drawerWidth,
  },
  'content-right': {
    marginRight: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'contentShift-left': {
    marginLeft: 0,
  },
  'contentShift-right': {
    marginRight: 0,
  },
});
class Body extends React.Component {

  state = {
    openDrawer: false,
  };

  handleDrawerOpen = () => {
    this.setState({ openDrawer: !this.state.openDrawer });
  };

  handleDrawerClose = () => {
    this.setState({ openDrawer: false });
  };

  render() {
    const { classes, theme } = this.props;
    const { openDrawer } = this.state;

    return (
      <div>
        <div className={classes.root}>
          <div className={classes.appFrame}>
            <AppBar position="static"
              className={classNames(classes.appBar, {
                [classes.appBarShift]: openDrawer,
                [classes[`appBarShift-left`]]: openDrawer,
              })}>
              <AppToolbar handleDrawerOpen={this.handleDrawerOpen} />
            </AppBar>
            <AppDrawer open={openDrawer} handleDrawerClose={this.handleDrawerClose} />
            <main
              className={classNames(classes.content, classes[`content-left`], {
                [classes.contentShift]: openDrawer,
                [classes[`contentShift-left`]]: openDrawer,
              })}>
              <RouteBuilder {...this.props} />
            </main>
          </div>
        </div>
      </div>
    );
  }
}

Body.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

module.exports = withStyles(styles, { withTheme: true })(Body);
