
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { IntlProvider, addLocaleData } from 'react-intl'
import en from 'react-intl/locale-data/en'
import hu from 'react-intl/locale-data/hu';

addLocaleData([...en, ...hu])

class IntlProviderWrapper extends Component {
    constructor(props) {
        super(props);
        const { initialLocale: locale, initialMessages: messages } = props;
        this.state = { locale, messages };
    }
    render() {return <IntlProvider {...this.props}>{this.props.children}</IntlProvider>}
}

let mapStateToProps = (state) => ({ locale: state.localesReducer.locale, messages: state.localesReducer.messages })

export default connect(mapStateToProps)(IntlProviderWrapper)